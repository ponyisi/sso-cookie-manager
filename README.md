# sso-cookie-manager

A helper image to maintain an SSO cookie from kerberos credentials (assumes you run [kerberos-ticket-manager](https://gitlab.cern.ch/paas-tools/kerberos-ticket-manager) also). For applications that need to authenticate to web services.
