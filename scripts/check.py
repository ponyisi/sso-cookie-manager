#!/usr/bin/env python3

import os
import http.cookiejar
import sys

cj=http.cookiejar.MozillaCookieJar(os.environ['SSO_COOKIE_FILE'])
cj.load()  # throws exception if file not found
if any(_.is_expired() for _ in cj):
    sys.exit(1)
else:
    sys.exit(0)