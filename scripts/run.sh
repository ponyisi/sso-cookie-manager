#!/bin/sh

mode=$1
case "${mode}" in
  "once")
    auth-get-sso-cookie -u ${SSO_URL} -o ${SSO_COOKIE_FILE}
    ;;
  "daemon")
    export TERM=xterm
    watch -t -n 3600 'auth-get-sso-cookie -u ${SSO_URL} -o ${SSO_COOKIE_FILE}.new && mv ${SSO_COOKIE_FILE}.new ${SSO_COOKIE_FILE}'
    ;;
  *)
    echo "Invalid mode: $mode. Supported modes are 'once' and 'daemon'" 1>&2
    exit 1
    ;;
esac

