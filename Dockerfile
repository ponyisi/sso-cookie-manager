FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

# install packages
RUN yum-config-manager --add-repo https://linuxsoft.cern.ch/internal/repos/authz7-stable/x86_64/os/ \\
    && yum install auth-get-sso-cookie -y \\
    && yum clean all 

COPY scripts/* /

RUN chmod +x /run.sh /check.py

ENTRYPOINT ["/run.sh"]

CMD [ "daemon" ]
